+++
title = "libhandy"
description = "Building blocks for modern adaptive GNOME apps"
date = 2021-09-07T08:50:45+00:00
updated = 2021-09-07T08:50:45+00:00
draft = false
+++

**libhandy** describes itself as "Building blocks for modern adaptive GNOME apps". It is to [GTK+ 3](@/frameworks/gtk3.md) what [libadwaita](@/frameworks/libadwaita.md) is to [GTK4](@/frameworks/gtk4.md). It was originally developed by [Purism](https://puri.sm) for the Librem 5 Phone. 

* [Docs](https://gnome.pages.gitlab.gnome.org/libhandy/)
* [Source](https://gitlab.gnome.org/GNOME/libhandy/)