+++
title = "Lemmur"
description = "A mobile client for Lemmy - a federated reddit alternative"
aliases = []
date = 2023-01-07

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Filip Krawczyk",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Flutter",]
backends = []
services = [ "Lemmy",]
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Network",]
programming_languages = [ "Dart",]
build_systems = [ "flutter",]

[extra]
repository = "https://github.com/LemmurOrg/lemmur"
homepage = "https://github.com/LemmurOrg/lemmur"
bugtracker = "https://github.com/LemmurOrg/lemmur/issues"
donations = "https://www.buymeacoffee.com/lemmur"
translations = "https://weblate.join-lemmy.org/engage/lemmur"
more_information = [ "https://lemmy.ml/post/677871",]
summary_source_url = "https://github.com/LemmurOrg/lemmur"
screenshots = [ "https://f-droid.org/repo/com.krawieck.lemmur/en-US/phoneScreenshots/1.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.krawieck.lemmur/1.png", "https://img.linuxphoneapps.org/com.krawieck.lemmur/2.png", "https://img.linuxphoneapps.org/com.krawieck.lemmur/3.png", "https://img.linuxphoneapps.org/com.krawieck.lemmur/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.krawieck.lemmur"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "lemmur",]
appstream_xml_url = "https://raw.githubusercontent.com/LouisMarotta/lemmur/flatpak-linux/flatpak/com.krawieck.lemmur.metainfo.xml"
reported_by = "linmob"
updated_by = ""
+++



### Description

Lemmur aims to provide a seamless experience when browsing different Lemmy instances. You can have multiple multiple instances added at the same time without having to awkwardly switch between them. [Source](https://raw.githubusercontent.com/LouisMarotta/lemmur/flatpak-linux/flatpak/com.krawieck.lemmur.metainfo.xml)


### Notice

Flatpak support is pending, see pull request: https://github.com/LemmurOrg/lemmur/pull/346