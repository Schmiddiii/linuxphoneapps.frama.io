+++
title = "Chatty"
description = "XMPP and SMS messaging via libpurple and Modemmanager"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Purism",]
categories = [ "SMS", "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = [ "libpurple", "ModemManager",]
services = [ "SMS", "MMS", "XMPP", "Matrix",]
packaged_in = [ "alpine_3_17", "alpine_edge", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://source.puri.sm/Librem5/chatty"
homepage = ""
bugtracker = "https://source.puri.sm/Librem5/chatty/-/issues/"
donations = ""
translations = ""
more_information = [ "https://wiki.mobian.org/doku.php?id=chatty",]
summary_source_url = "https://source.puri.sm/Librem5/chatty"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "sm.puri.Chatty"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "purism-chatty",]
appstream_xml_url = "https://source.puri.sm/Librem5/chatty/-/raw/master/data/sm.puri.Chatty.metainfo.xml.in"
reported_by = "cahfofpai"
updated_by = "script"
+++
