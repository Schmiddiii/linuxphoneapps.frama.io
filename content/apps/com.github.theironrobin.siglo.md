+++
title = "siglo"
description = "GTK app to sync InfiniTime watch with PinePhone"
aliases = []
date = 2021-02-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alex R",]
categories = [ "watch companion",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/theironrobin/siglo"
homepage = ""
bugtracker = "https://github.com/theironrobin/siglo/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/theironrobin/siglo"
screenshots = [ "https://ironrobin.net/images/siglo-screenshot-1.png", "https://ironrobin.net/images/siglo-screenshot-2.png", "https://ironrobin.net/images/siglo-screenshot-3.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.theironrobin.siglo"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.theironrobin.siglo"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "siglo",]
appstream_xml_url = "https://raw.githubusercontent.com/theironrobin/siglo/main/data/com.github.theironrobin.siglo.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++



### Description

Features: - Scan for one or more InfiniTime devices, - Sync the time, - Update your firmware – either manually or directly from the InfiniTime release page, - Optionally Keep Paired for Chatty notifications (currently broken in flatpak). Supports all Phosh-based PinePhone distros. [Source](https://raw.githubusercontent.com/theironrobin/siglo/main/data/com.github.theironrobin.siglo.appdata.xml.in)


### Notice

Great companion app for updating and time syncing InifiniTime, e.g. on the PineTime smartwatch.
