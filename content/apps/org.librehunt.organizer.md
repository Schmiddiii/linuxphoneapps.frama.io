+++
title = "Organizer"
description = "Organizer is a GTK Python app to organize your files into neat categories! It does so by the filetype."
aliases = []
date = 2019-02-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "file management",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "System", "FileTools",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/aviwad/organizer"
homepage = ""
bugtracker = "https://gitlab.gnome.org/aviwad/organizer/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/aviwad/organizer"
screenshots = [ "https://gitlab.gnome.org/aviwad/organizer",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.librehunt.Organizer"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.librehunt.Organizer"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "organizer",]
appstream_xml_url = "https://gitlab.gnome.org/aviwad/organizer/-/raw/master/data/org.librehunt.Organizer.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"
+++

