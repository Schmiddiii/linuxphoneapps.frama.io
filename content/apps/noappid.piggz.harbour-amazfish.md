+++
title = "Amazfish"
description = "Amazfish Bip support for SailfishOS"
aliases = []
date = 2021-05-05
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "piggz",]
categories = [ "watch companion",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "aur", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/piggz/harbour-amazfish"
homepage = ""
bugtracker = "https://github.com/piggz/harbour-amazfish/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/piggz/harbour-amazfish"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "amazfish",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++




### Notice

Companion app for smartwatches, including PineTime and Bangle.js. Originally for SailfishOS, has been ported to Kirigami.
