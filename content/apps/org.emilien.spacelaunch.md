+++
title = "Space Launch"
description = "When will the next rocket soar to the skies?"
aliases = []
date = 2021-11-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Emilien Lescoute",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "spacelaunchnow.me",]
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/elescoute/spacelaunch"
homepage = ""
bugtracker = "https://gitlab.com/elescoute/spacelaunch/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/org.emilien.SpaceLaunch"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.emilien.SpaceLaunch"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.emilien.SpaceLaunch"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.com/elescoute/spacelaunch/-/raw/main/data/org.emilien.SpaceLaunch.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

With Space Launch you can keep track of upcoming launches. Data on the launches is provided by spacelaunchnow.me. Space Launch is currently in alpha release. More features will be added later. Compatible with Linux Phone using Phosh (PinePhone, Librem 5). [Source](https://flathub.org/apps/org.emilien.SpaceLaunch)


### Notice

This app has a great custom animation while loading data.
