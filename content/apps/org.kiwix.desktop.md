+++
title = "Kiwix Desktop"
description = "The Kiwix Desktop is a viewer/manager of ZIM files for GNU/Linux and Microsoft Windows Oses."
aliases = []
date = 2020-12-12
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "The Kiwix Foundation",]
categories = [ "education",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "QtWidgets",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Qt", "Education",]
programming_languages = [ "JavaScript", "Cpp",]
build_systems = [ "qmake",]

[extra]
repository = "https://github.com/kiwix/kiwix-desktop"
homepage = "https://www.kiwix.org/en/"
bugtracker = "https://github.com/kiwix/kiwix-desktop/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/kiwix/kiwix-desktop"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kiwix.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kiwix.desktop"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kiwix-desktop",]
appstream_xml_url = "https://raw.githubusercontent.com/kiwix/kiwix-desktop/master/resources/org.kiwix.desktop.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Some menu items are not accessible, but it mostly works.
