+++
title = "List"
description = "Focus on your tasks."
aliases = []
date = 2023-04-17
updated = 2023-04-22

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Vlad Krupinski",]
categories = [ "productivity",]
mobile_compatibility = [ "3",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita", ]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GNOME", "GTK", "Utility"]
programming_languages = [ "C",]
build_systems = [ "make",]

[extra]
repository = "https://github.com/mrvladus/List"
homepage = "https://github.com/mrvladus/List"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = ""
screenshots = [ "https://github.com/mrvladus/List/blob/ea746ec9d9573b7a3ec34e56ac8ea8826ad7868c/screenshots/light.png?raw=true", "https://github.com/mrvladus/List/blob/ea746ec9d9573b7a3ec34e56ac8ea8826ad7868c/screenshots/dark.png?raw=true",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.mrvladus.list/1.png", "https://img.linuxphoneapps.org/io.github.mrvladus.list/2.png", "https://img.linuxphoneapps.org/io.github.mrvladus.list/3.png", "https://img.linuxphoneapps.org/io.github.mrvladus.list/4.png",]
all_features_touch = 1
intended_for_mobile = 0 
app_id = "io.github.mrvladus.List"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.mrvladus.List"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/mrvladus/List/main/io.github.mrvladus.List.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/mrvladus/List/main/data/io.github.mrvladus.List.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"

+++

### Description

Todo application for those who prefer simplicity. Add new todo's, remove comleted, nothing more. [Source](https://raw.githubusercontent.com/mrvladus/List/main/data/io.github.mrvladus.List.metainfo.xml)

### Notice

Too wide since 44.2, screenshots show 44.1.
