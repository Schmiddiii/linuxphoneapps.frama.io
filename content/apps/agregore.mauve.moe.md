+++
title = "Agregore Browser"
description = "A minimal browser for the distributed web"
aliases = []
date = 2021-05-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = []
app_author = [ "agregoreweb",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Electron",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Network", "WebBrowser",]
programming_languages = [ "JavaScript",]
build_systems = [ "yarn",]

[extra]
repository = "https://github.com/AgregoreWeb/agregore-browser"
homepage = "https://agregore.mauve.moe/"
bugtracker = "https://github.com/AgregoreWeb/agregore-browser/issues/"
donations = ""
translations = ""
more_information = [ "https://www.youtube.com/watch?v=TnYKvOQB0ts", "https://github.com/AgregoreWeb/agregore-browser/issues/103",]
summary_source_url = "https://github.com/AgregoreWeb/agregore-browser"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "agregore.mauve.moe"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "agregore-browser",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++




### Notice

Build it as described on https://github.com/AgregoreWeb/agregore-browser/issues/103
