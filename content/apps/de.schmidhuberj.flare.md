+++
title = "Flare"
description = "A unofficial Signal GTK client."
aliases = []
date = 2022-10-08
updated = 2023-02-21

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Julian Schmidhuber",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "presage",]
services = [ "Signal",]
packaged_in = [ "alpine_edge", "flathub", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/Schmiddiii/flare"
homepage = ""
bugtracker = "https://gitlab.com/Schmiddiii/flare/-/issues/"
donations = "https://gitlab.com/Schmiddiii/flare#donate"
translations = "https://hosted.weblate.org/engage/flare/"
more_information = []
summary_source_url = "https://gitlab.com/Schmiddiii/flare"
screenshots = [ "https://gitlab.com/Schmiddiii/flare/-/raw/master/data/screenshots/screenshot.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/de.schmidhuberj.flare/1.png", "https://img.linuxphoneapps.org/de.schmidhuberj.flare/2.png", "https://img.linuxphoneapps.org/de.schmidhuberj.flare/3.png", "https://img.linuxphoneapps.org/de.schmidhuberj.flare/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "de.schmidhuberj.Flare"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.schmidhuberj.Flare"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "flare-signal-client",]
appstream_xml_url = "https://gitlab.com/Schmiddiii/flare/-/raw/master/data/de.schmidhuberj.Flare.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

A unofficial GTK client for the messaging application Signal. This is a very simple application with many features missing compared to the official applications.      Link device     Send messages     Receive messages     Reply to a message     React to a message     Sending and receiving attachments - Encrypted storage  Please note that using this application will probably worsen your security compared to using official Signal applications. Use with care when handling sensitive data. Look at the projects README for more information about security. [Source](https://gitlab.com/Schmiddiii/flare/-/raw/master/data/de.schmidhuberj.Flare.metainfo.xml)


### Notice

Native Signal Desktop alternative.
