+++
title = "GNOME Chess"
description = "Play the classic two-player board game of chess"
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]

[extra]
reported_by = "Moxvallix"
verified = "✅"
repository = "https://gitlab.gnome.org/GNOME/gnome-chess"
homepage = "https://wiki.gnome.org/Apps/Chess"
more_information = []
summary_source_url = "https://flathub.org/apps/org.gnome.Chess"
screenshots = []
screenshots_img = []
app_id = "org.gnome.Chess"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Chess"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-chess",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-chess/-/raw/master/data/org.gnome.Chess.appdata.xml.in"

+++
