# linuxphoneapps.org

## What is this?

A [Zola](https://getzola.org)-based (with a heavily modified [adidoks](https://www.getzola.org/themes/adidoks/) theme) replacement for [LINMOBapps.frama.io](https://linmobapps.frama.io) - the goal is an app directory for Linux Phones like the PinePhone and Librem 5, which 
* performs well on these phones (linmobapps did not) ☑️,
* allows for easier contributions for developers and users,
* provides more information and requires less maintenance.

It's up and runnning at [linuxphoneapps.org](https://linuxphoneapps.org)!

Please join this project and contribute!

## FAQ

### How to get this running locally to hack on it?

* [Install the latest release of Zola](https://www.getzola.org/documentation/getting-started/installation/) ([Linux Distro packaging status](https://repology.org/project/zola/versions))
* Clone this repo `git clone https://github.com/linuxphoneapps/linuxphoneapps.org.git` (or fork it first and ssh clone your fork)
* `cd linuxphoneapps.org`
* Install the adidoks theme: 
  ~~~
  `git submodule init` 
  `git submodule update`
  ~~~
* After that, running `zola serve` should get the site up and running - without apps, see `gitlab-ci.yml` for now!

### How does this roughly work?

* `content` contains subfolders for the content, e.g. apps, docs or further info on taxonomies
* `templates` contains the templates that render that content. For building templates, check out the documentation for [Zola](https://www.getzola.org/documentation/getting-started/overview/) and [Tera](https://tera.netlify.app/docs/), Zola's templating language.
* `config.toml` defines a bunch of base variables for the site,
* `themes` is not a working directory, but only the place the adidoks theme gets cloned to. 

### How to add apps or games?

See [Contributing](https://linuxphoneapps.org/docs/contributing/), mainly [Submit Apps on GitLab](https://linuxphoneapps.org/docs/contributing/submit-app-on-gitlab/) and [Listings explained](https://linuxphoneapps.org/docs/contributing/listings-explained/).

App listings live in content/apps/,
game listings live in content/games/.

### Remaining roadmap (see previous revisions of this document for more)

#### 3. Pie in the Sky

* Web app for Linux Phones (preferable with a UX as [Oliver Smith describes in "Linux Mobile vs. The Social Dilemma" (minute ~ 18:00)](https://fosdem.org/2022/schedule/event/mobile_social_dilemma/))
* package repositories
* ...
